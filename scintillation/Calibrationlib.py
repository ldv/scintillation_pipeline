import numpy
import numpy as np
from astropy.convolution import interpolate_replace_nans
import astropy.units as u
from astropy.time import Time
from astropy.coordinates import EarthLocation, ITRS,AltAz
from scipy.interpolate import interp1d
from scipy.ndimage import median_filter
from scipy.ndimage.filters import uniform_filter1d
from scipy.signal import butter,filtfilt,fftconvolve
import lofarantpos.geo as lofargeo
from lofarantpos.db import LofarAntennaDatabase
R_earth=6364.62*u.km;
R_earth_val=R_earth.to("m").value;


def model_flux(calibrator, frequency):
    '''
    Calculates the model matrix for flux calibration for a range of known calibrators:
    J0133-3629, 3C48, Fornax A, 3C 123, J0444+2809, 3C138, Pictor A, Taurus A, 3C147, 3C196, Hydra A, Virgo A, 
    3C286, 3C295, Hercules A, 3C353, 3C380, Cygnus A, 3C444, Cassiopeia A

    Input: the calibrator name, frequency range, and time range
    Output: the calibration matrix (in Jy)
    Code adapted original from Peijin Zhang
    '''
    parameters = []

    Cal_dict = {'J0133-3629': [1.0440, -0.662, -0.225],
                '3C48': [1.3253, -0.7553, -0.1914, 0.0498],
                'For A': [2.218, -0.661],
                'ForA': [2.218, -0.661],
                '3C123': [1.8017, -0.7884, -0.1035, -0.0248, 0.0090],
                'J0444-2809': [0.9710, -0.894, -0.118],
                '3C138': [1.0088, -0.4981, -0.155, -0.010, 0.022, ],
                'Pic A': [1.9380, -0.7470, -0.074],
                'Tau A': [2.9516, -0.217, -0.047, -0.067],
                'PicA': [1.9380, -0.7470, -0.074],
                'TauA': [2.9516, -0.217, -0.047, -0.067],
                '3C147': [1.4516, -0.6961, -0.201, 0.064, -0.046, 0.029],
                '3C196': [1.2872, -0.8530, -0.153, -0.0200, 0.0201],
                'Hyd A': [1.7795, -0.9176, -0.084, -0.0139, 0.030],
                'Vir A': [2.4466, -0.8116, -0.048],
                'HydA': [1.7795, -0.9176, -0.084, -0.0139, 0.030],
                'VirA': [2.4466, -0.8116, -0.048],
                '3C286': [1.2481, -0.4507, -0.1798, 0.0357],
                '3C295': [1.4701, -0.7658, -0.2780, -0.0347, 0.0399],
                'Her A': [1.8298, -1.0247, -0.0951],
                'HerA': [1.8298, -1.0247, -0.0951],
                '3C353': [1.8627, -0.6938, -0.100, -0.032],
                '3C380': [1.2320, -0.791, 0.095, 0.098, -0.18, -0.16],
                'Cyg A': [3.3498, -1.0022, -0.225, 0.023, 0.043],
                'CygA': [3.3498, -1.0022, -0.225, 0.023, 0.043],
                '3C444': [3.3498, -1.0022, -0.22, 0.023, 0.043],
                'Cas A': [3.3584, -0.7518, -0.035, -0.071],
                'CasA': [3.3584, -0.7518, -0.035, -0.071]}
    if calibrator in Cal_dict.keys():
        parameters = Cal_dict[calibrator]
    else:
        parameters = [1., 0.]
        # raise ValueError(calibrator, "is not in the calibrators list")

    flux_model = 0
    freqs = frequency * 1.e-3  # convert from MHz to GHz
    for j, p in enumerate(parameters):
        flux_model += p * np.log10(freqs) ** j
    flux_model = 10 ** flux_model  # because at first the flux is in log10
    return flux_model


def filter_rfi(data, nsigma=5, ntpts=49, nchans=5):
    '''
    RFI mitigation strategy for dynamic spectra:
    - median filter the data, in frequency dimension only
    - flatten data by 10th percentile
    - mitigate remaining spikes by standard deviation check across pass-band for each time sample
    Assumes data are transposed such that [x,y] are [time,frequency].
    Input: 
    - A 2-D array of [time,frequency]
    - number of sigma to use in thresholding
    - Number of time points for flattening data, which must be low enough to not flatten also the scintillation
    - Number of frequency channels for additional flattening of the data
    Output: An array with flags
    '''

    ntimepts, nfreqs = data.shape

    # flatten = median_filter(data,(ntpts,1),mode='constant',cval=1)
    # faster to median filter every Nsamples and interpolate
    cutoff = ntimepts - ntimepts % ntpts
    medfilter = np.median(data[:cutoff].reshape((-1, ntpts, nfreqs)), axis=1)
    xnew = np.arange(ntimepts)
    x = xnew[:cutoff][::ntpts]
    f = interp1d(x, medfilter, axis=0, fill_value='extrapolate')  # interpolate to all skipped samples
    flatten = f(xnew)
    flatten = median_filter(flatten, (1, nchans), mode='constant', cval=1)
    flatdata = data / flatten
    nanmed = np.nanmedian(flatdata)
    diff = abs(flatdata - nanmed)
    sd = np.nanmedian(diff)  # Median absolute deviation
    #also flag zero valued data
    flags = data==0
    maskdata = np.where(np.logical_or(diff > sd * nsigma,flags), 1, 0)
    
    return maskdata


def apply_bandpass(data, freqs, freqaxis=0, timeaxis=1, target="Cas A", sample_rate=180, filter_length=300,
                   rfi_filter_length=49, flag_value=5, replace_value=1):
    '''apply a bandpass correction to the data, scaling the data to the nominal flux of the calibrator, also flag outliers and replace flagged data with replace_value
    Input:
    data: the numpy array with the data
    freqs: numpy array of the freqs (MHz,for flux calibration)
    freqaxis: frequency axis
    timeaxis: time axis 
    target: name of the target (for flux calibration)
    sample_rate: sample the data with this rate to speed up filtering
    filter_length: lenght of the gaussian filter, typically half an hour for beam changes  
    flag_value: flag data above flag_value times nominal value
    replace_value: replace flagged data with this value, if "interpolate" replace with the model flux

    Output:
    scaled data
    array of flags
    array of flux values
    
    '''

    flux = model_flux(target, freqs)
    data = np.swapaxes(data, 0, timeaxis)

    # medfilter = gaussian_filter1d(data[::sample_rate], axis=0 , sigma=filter_length) #or use median_filter?
    medfilter = median_filter(data[::sample_rate],
                              size=(filter_length,) + (1,) * (len(data.shape) - 1))  # or use gaussian_filter?
    xnew = np.arange(data.shape[0])
    x = xnew[::sample_rate]
    f = interp1d(x, medfilter, axis=0, fill_value='extrapolate')  # interpolate to all skipped samples
    bandpass = f(xnew)
    newshape = [1, ] * len(data.shape)
    if freqaxis == 0:
        newshape[timeaxis] = flux.shape[0]
    else:
        newshape[freqaxis] = flux.shape[0]
    bandpass[bandpass == 0] = 1
    data /= bandpass
    flags = filter_rfi(data, nsigma=flag_value, ntpts=rfi_filter_length, nchans=5)
    tmpdata = np.copy(data)
    if replace_value == "interpolate":
        tmpdata[flags > 0] = np.nan
        tmpdata = interpolate_replace_nans(tmpdata, np.ones((21, 21)))
        tmpdata[np.isnan(tmpdata)] = 1
    else:

        tmpdata[flags > 0] = replace_value

    return np.swapaxes(tmpdata, 0, timeaxis), np.swapaxes(flags, 0, timeaxis), flux, np.swapaxes(bandpass, 0, timeaxis)


def getS4_fast(data, window_size, skip_sample_time=65, axis=1, has_nan=False):
    '''Calculate S4 value for data along axis, Could be fast if it works with numba
    Input:
    data: numpy array with data (maximum resolution but RFI flagged)
    window_size: int: the window to calculate S4, typically 60s and 180s
    skip_sample_time: int: only calculate S4 every skip_sample_time (in samples, typically 1s)
    has_nan: boolean, if True use the much slower nanmean function to ignore nans
    stepsize: int, size of step through the data to reduce memorey usage
    output:
    numpy array with S4 values
    new times axis array
    '''
    # make sure axis to sample = 0-th axis:
    tmpdata = np.swapaxes(data, 0, axis)
    ntimes = tmpdata.shape[0]
    indices = np.arange(window_size)

    idx_step = np.arange(0, ntimes - window_size, skip_sample_time)

    idx = idx_step.reshape((-1, 1)) + indices.reshape((1, -1))
    S4 = np.zeros((idx.shape[0],) + tmpdata.shape[1:], dtype=tmpdata.dtype)
    if has_nan:
        for i in range(idx.shape[0]):
            for j in range(window_size):
                avgsqdata = np.sum(tmpdata[idx[i]] ** 2, axis=0) / (
                        window_size - np.sum(np.isnan(tmpdata[idx[i]]), axis=0))
                avgdatasq = (np.sum(tmpdata[idx[i]], axis=0) / (
                        window_size - np.sum(np.isnan(tmpdata[idx[i]]), axis=0))) ** 2
            S4[i] = np.sqrt((avgsqdata - avgdatasq) / avgdatasq)
    else:
        for i in range(idx.shape[0]):
            avgsqdata = np.sum(tmpdata[idx[i]] ** 2, axis=0) / window_size
            avgdatasq = np.sum(tmpdata[idx[i]], axis=0) ** 2 / window_size ** 2
            S4[i] = np.sqrt((avgsqdata - avgdatasq) / avgdatasq)

    return np.swapaxes(S4, 0, axis)


def window_stdv_mean(arr, window):
    # Cool trick: you can compute the standard deviation
    # given just the sum of squared values and the sum of values in the window.
    c1 = uniform_filter1d(arr, window, mode='constant', axis=0)
    c2 = uniform_filter1d(arr * arr, window, mode='constant', axis=0)
    return (np.abs(c2 - c1 * c1) ** .5)[window // 2:-window // 2 + 1], c1[window // 2:-window // 2 + 1]


def getS4(data, window_size, skip_sample_time=65, axis=1, has_nan=False):
    '''Calculate S4 value for data along axis
    Input:
    data: numpy array with data (maximum resolution but RFI flagged)
    window_size: int: the window to calculate S4, typically 60s and 180s
    skip_sample_time: int: only calculate S4 every skip_sample_time (in samples, typically 1s)
    has_nan: boolean, if True use the much slower nanmean function to ignore nans
    stepsize: int, size of step through the data to reduce memorey usage
    output:
    numpy array with S4 values
    new times axis array
    '''
    # make sure axis to sample = 0-th axis:
    tmpdata = np.swapaxes(data, 0, axis)
    stddata, avgdata = window_stdv_mean(tmpdata, window_size)
    S4 = stddata[::skip_sample_time] / avgdata[::skip_sample_time]
    return np.swapaxes(S4, 0, axis)


from numba import guvectorize


@guvectorize(['void(float32[:], intp[:], float32[:])'], '(n),() -> (n)')
def move_mean(a, window_arr, out):
    window_width = window_arr[0]
    asum = 0.0
    count = 0
    for i in range(window_width):
        asum += a[i]
        count += 1
        out[i] = asum / count
    for i in range(window_width, len(a)):
        asum += a[i] - a[i - window_width]
        out[i] = asum / count


@guvectorize(['void(float32[:], intp[:], float32[:])'], '(n),() -> (n)')
def move_mean_sq(a, window_arr, out):
    window_width = window_arr[0]
    asum = 0.0
    count = 0
    for i in range(window_width):
        asum += numpy.power(a[i], 2)
        count += 1
        out[i] = asum / count
    for i in range(window_width, len(a)):
        asum += numpy.power(a[i], 2) - numpy.power(a[i - window_width], 2)
        out[i] = asum / count


def window_stdv_mean_numba(arr: numpy.ndarray, window):
    arr = numpy.swapaxes(arr, 0, 1)
    start_index = window // 2
    mean_arr = move_mean(arr, window)
    std_arr = numpy.sqrt(numpy.abs(move_mean_sq(arr, window) - numpy.power(mean_arr, 2)))

    mean_arr = np.swapaxes(mean_arr, 0, 1)
    std_arr = np.swapaxes(std_arr, 0, 1)

    return std_arr[start_index: -start_index + 1], mean_arr[start_index: -start_index + 1]


def getS4Numba(data, window_size, skip_sample_time=65, axis=1, has_nan=False):
    '''Calculate S4 value for data along axis
    Input:
    data: numpy array with data (maximum resolution but RFI flagged)
    window_size: int: the window to calculate S4, typically 60s and 180s
    skip_sample_time: int: only calculate S4 every skip_sample_time (in samples, typically 1s)
    has_nan: boolean, if True use the much slower nanmean function to ignore nans
    stepsize: int, size of step through the data to reduce memory usage
    output:
    numpy array with S4 values
    new times axis array
    '''
    # make sure axis to sample = 0-th axis:
    tmpdata = np.swapaxes(data, 0, axis)
    stddata, avgdata = window_stdv_mean_numba(tmpdata, window_size)
    S4 = stddata[::skip_sample_time] / avgdata[::skip_sample_time]
    return np.swapaxes(S4, 0, axis)


def getS4Naive(data, window_size, skip_sample_time=65, axis=1, has_nan=False):
    '''Calculate S4 value for data along axis
    Input:
    data: numpy array with data (maximum resolution but RFI flagged)
    window_size: int: the window to calculate S4, typically 60s and 180s
    skip_sample_time: int: only calculate S4 every skip_sample_time (in samples, typically 1s)
    has_nan: boolean, if True use the much slower nanmean function to ignore nans
    stepsize: int, size of step through the data to reduce memorey usage
    output:
    numpy array with S4 values
    new times axis array
    '''
    # make sure axis to sample = 0-th axis:
    tmpdata = np.swapaxes(data, 0, axis).astype(np.float32).copy()
    ntimes, n_freq = tmpdata.shape
    mean_arr = np.zeros((ntimes, n_freq), dtype=np.float32)
    std_arr = np.zeros((ntimes, n_freq), dtype=np.float32)
    naive_running_std(tmpdata, window_size, mean_arr, std_arr)
    S4 = std_arr[::skip_sample_time] / mean_arr[::skip_sample_time]
    return np.swapaxes(S4, 0, axis)


def getS4_slow(data, window_size, skip_sample_time=65, axis=1, has_nan=False):
    '''Calculate S4 value for data along axis
    Input:
    data: numpy array with data (maximum resolution but RFI flagged)
    window_size: int: the window to calculate S4, typically 60s and 180s
    skip_sample_time: int: only calculate S4 every skip_sample_time (in samples, typically 1s)
    has_nan: boolean, if True use the much slower nanmean function to ignore nans
    stepsize: int, size of step through the data to reduce memorey usage
    output:
    numpy array with S4 values
    new times axis array
    '''
    # make sure axis to sample = 0-th axis:
    tmpdata = np.swapaxes(data, 0, axis)
    ntimes = tmpdata.shape[0]
    slides = sliding_window_view(tmpdata, window_size, axis=0)[::skip_sample_time]
    if has_nan:
        avgsqdata = np.nanmean(slides ** 2, axis=-1)
        avgdatasq = np.nanmean(slides, axis=-1) ** 2
    else:
        avgsqdata = np.mean(slides ** 2, axis=-1)
        avgdatasq = np.mean(slides, axis=-1) ** 2
    S4 = np.sqrt(np.abs(avgsqdata - avgdatasq) / avgdatasq)
    return np.swapaxes(S4, 0, axis)


def getS4_medium(data, window_size, skip_sample_time=65, axis=1, has_nan=False):
    '''Calculate S4 value for data along axis
    Input:
    data: numpy array with data (maximum resolution but RFI flagged)
    window_size: int: the window to calculate S4, typically 60s and 180s
    skip_sample_time: int: only calculate S4 every skip_sample_time (in samples, typically 1s)
    has_nan: boolean, if True use the much slower nanmean function to ignore nans
    stepsize: int, size of step through the data to reduce memorey usage
    output:
    numpy array with S4 values
    new times axis array
    '''
    # make sure axis to sample = 0-th axis:
    tmpdata = np.swapaxes(data, 0, axis)
    ntimes = tmpdata.shape[0]
    indices = np.arange(window_size)

    idx_step = np.arange(0, ntimes - window_size, skip_sample_time)

    idx = idx_step[:, np.newaxis] + indices[np.newaxis]

    S4 = []
    if has_nan:
        for i in range(idx.shape[0]):
            avgsqdata = np.nanmean(tmpdata[idx[i]] ** 2, axis=0)
            avgdatasq = np.nanmean(tmpdata[idx[i]], axis=0) ** 2
            S4.append(np.sqrt(np.abs(avgsqdata - avgdatasq) / avgdatasq))
    else:
        for i in range(idx.shape[0]):
            avgsqdata = np.mean(tmpdata[idx[i]] ** 2, axis=0)
            avgdatasq = np.mean(tmpdata[idx[i]], axis=0) ** 2
            S4.append(np.sqrt(np.abs(avgsqdata - avgdatasq) / avgdatasq))

    S4 = np.array(S4)
    return np.swapaxes(S4, 0, axis)


def getMedian(data, Nsamples, axis=0, flags=None, bandpass=None, has_nan=False):
    '''average the data using median over Nsamples samples, ignore last samples <Nsamples. If has_nan, use the much slower nanmedian function'''
    # flags is None or has same shape as data
    # make sure average axis is first
    tmpdata = np.swapaxes(data, 0, axis)
    cutoff = (tmpdata.shape[0] // Nsamples) * Nsamples

    tmpdata = tmpdata[:cutoff].reshape((-1, Nsamples) + tmpdata.shape[1:])
    if has_nan:
        avgdata = np.nanmedian(tmpdata, axis=1)
    else:
        avgdata = np.median(tmpdata, axis=1)
    if not flags is None:
        flags = np.swapaxes(flags, 0, axis).astype(float)
        flags = np.average(flags[:cutoff].reshape((-1, Nsamples) + avgdata.shape[1:]), axis=1)
    if not bandpass is None:
        bandpass = np.swapaxes(bandpass, 0, axis).astype(float)
        bandpass = np.average(bandpass[:cutoff].reshape((-1, Nsamples) + avgdata.shape[1:]), axis=1)

    return np.swapaxes(avgdata, 0, axis), np.swapaxes(flags, 0, axis), np.swapaxes(bandpass, 0, axis)  # swap back




def get_alpha_IPP(c,stat_pos,directions):
    '''Helperfunction to calculate factor alpha.
    c is an array with shape heights. directions has shape times x 3
    returns alpha(heights x directions)'''
    inp_rs = np.dot(directions,stat_pos)  #shape (times)
    b = inp_rs[np.newaxis]  # already divided by 2
    alphas = -b + np.sqrt(b**2-c.to("m2").value[:,np.newaxis])
    return alphas

def getIPP(heights,statpos_array,times,delays,source):
    '''get IPP using the simple spherical model, for a single skycoord
    an array of ITRF station position and delays per station, an array of altitudes and times.
    delays should have shape (stations x times)'''
    c = R_earth**2 - (R_earth + heights)**2
    if not type(times[0]) is Time:
        time = Time(times,format = 'mjd')
    pps = []
    for stat_pos,delay in zip(statpos_array,delays):
        if not type(stat_pos) is EarthLocation:
            stat_pos = EarthLocation.from_geocentric(*list(stat_pos),unit=u.m)

        stattimes =  times+delay
        aa = AltAz(location = stat_pos, obstime = stattimes)
        azel = source.transform_to(aa)
        directions = azel.transform_to(ITRS).cartesian.xyz.value.T
        
        posxyz = np.array((stat_pos.x.value,stat_pos.y.value,stat_pos.z.value))
        alphas = get_alpha_IPP(c,posxyz,directions)
        pps.append(posxyz[np.newaxis,np.newaxis] + alphas[:,:,np.newaxis]*directions[np.newaxis])
    return np.array(pps)

def etrs_to_local_north(xyz):
    return lofargeo.localnorth_to_etrs(xyz).swapaxes(-1,-2) #transpose of the matrices        

def get_dxdy(stations):
    
    #lofarantpos has the positions of all lofar stations. Use this method to get local ENU
    db = LofarAntennaDatabase()
    field_coords = np.array(
        [
            lofargeo.transform(
            [db.phase_centres[station]],
                db.phase_centres["CS002LBA"],
                db.pqr_to_etrs["CS002LBA"].T,
            )
            for station in stations
        ]
    )[:, :, :2].squeeze()
    uv_coords = field_coords[:, np.newaxis] - field_coords[np.newaxis]  #dx, dy coordinates
    return uv_coords

def transform(pp1,refpp,tmatrix):
    '''reimplementation of lofargeo.transform to allow for multidimensional arrays'''
    offset = pp1 - refpp
    offs = offset[...,np.newaxis]
    return np.matmul(tmatrix,offs)[...,0]

def butter_lowpass_filter(data, cutoff, fs, order=2):
    normal_cutoff = cutoff*2 / fs
    # Get the filter coefficients 
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    y = filtfilt(b, a, data)
    return y

def low_pass_filter(data,sample_time,cutoff=1):
    fs = 1./sample_time
    return butter_lowpass_filter(data, cutoff, fs, order=2)

    
def get_cross_corr(data,time_window,sample_time,sample_window):
    #apply high pass filter
    #data nstations x ntimes
    Ws = sample_window #number of samples to crosscorrelate (~180s)
    hw = time_window # timesteps through the data, should be equal or smaller than Ws
    
    cross_corr = []
    endframe = data.shape[1] - Ws
    if endframe<=0:
        endframe=1
    for window in range(0,endframe,hw):   
        ms1 = np.copy(data[:, np.newaxis, window:window + Ws])
        Ws = ms1.shape[-1] # reset Ws if shape of ms1 doesn't match
        ms1 -= np.average(ms1, axis=-1)[:, :, np.newaxis]
        ms2 = np.copy(data[np.newaxis,:, window:window + Ws][:,:,::-1])
        ms2 -= np.average(ms2, axis=-1)[:, :, np.newaxis]
        std1 = np.std(ms1, axis=-1)
        std2 = np.std(ms2, axis=-1)
        std1[std1==0] = 1
        std2[std2==0] = 1
        cross_corr.append(fftconvolve(ms1, ms2, mode='full', axes=-1) / ((Ws) * std1 * std2)[:,:,np.newaxis])
    lags = np.arange(-Ws + 1, Ws) * sample_time
    return np.moveaxis(np.array(cross_corr),0,-1),lags

def get_delays(cross_corr,lags,window=120):
    nsamples = cross_corr.shape[2]
    sample_time = lags[1] - lags[0]
    wmin = max(0, nsamples // 2 - int(round(0.5 * window / sample_time)))
    wmax = min(nsamples, nsamples // 2 + int(round(0.5 * window / sample_time)))+1
        
    nStat = cross_corr.shape[0]
    lag_idx = np.argmax(cross_corr[:,:,wmin:wmax],axis=2)
    fitted_lags = lags[wmin:wmax][lag_idx]
    #fitted_lags [0,1] positive means station 1 is lagging behind station 0!
    #lags = (np.argmax(cross_corr[:,:,wmin:wmax],axis=2)
    #        -cross_corr[:,:,wmin:wmax].shape[2]//2)*sample_time
    crval = np.max(cross_corr[:,:,wmin:wmax],axis=2)
    return fitted_lags,crval

def get_velocity(data, stations, sample_time, sample_window, starttime, source, heights):
    '''function to extract NS and EW velocities from crosscorrelations of LOFAR-CS beamformed data.
    Converts to pierpecepoints at different ionospheric heights to correct for tracking velocities.
    Memory intensive.
    Input:
    data : np.array  timeseries per station (nstations x ntimes)
    stations: list of str : LOFAR station names
    sample_time: float : sample time in seconds
    sample_window: float : time in seconds to sample velocities
    starttime: astropy.time.Time object
    source: astropy SkyCoord
    heights: astropy distance

    Output:
    allvelocities_nsew : np.array : velocities (times x heights x direction (NS,EW)  )
    cross_corr_0 : np.array : cross correlation at delay 0 : baselines x times
    delays : np.array : delays : baselines x times
    uvpp : np.array  : baselines x heights x 2 (projected at ionospheric layer)

    '''
    
    time_window_in_samples = int(round(sample_window/sample_time)) #number of times to investigate 10s
    window_size_multiplier = 5 # this amount of data goes into the crosscorrelation (typically 5min)
    cross_corr,lags = get_cross_corr(data,
                                     time_window_in_samples,
                                     sample_time,
                                     window_size_multiplier*time_window_in_samples)
    #remove any nans from cross_corr:
    cross_corr[np.isnan(cross_corr)]=0
    delays,crval = get_delays(cross_corr,lags)
    bad_stations = np.average(crval,axis=(1,2))<0.5
    selected_stations = [j for i,j in enumerate(stations) if not bad_stations[i]]
    if len(selected_stations)<4:
        return False, cross_corr[:,:,cross_corr.shape[2]//2], delays, False, False, False, False
               #enV , cross_corr0                           , delays, uvpp , refpp, crval, selected_stations
    good_stations = np.logical_not(bad_stations)
    delays = delays[good_stations][:,good_stations]
    crval = crval[good_stations][:,good_stations]
    cross_corr_0 = cross_corr[:,:,cross_corr.shape[2]//2]
    
    del cross_corr
    tri_idx = np.tril_indices(len(selected_stations),-1)
    cross_corr_0 = cross_corr_0[good_stations][:,good_stations][tri_idx[0],tri_idx[1]] #baselines x times
    delays = np.array(delays)[tri_idx[0],tri_idx[1]] #baselines x times
    nT = data.shape[-1]
    times = starttime + np.arange(0,
                                  nT-window_size_multiplier*time_window_in_samples,
                                  time_window_in_samples)*sample_time*u.s

    if not times.shape[0]:
        times = starttime +np.arange(1)*sample_time*u.s
    db = LofarAntennaDatabase()
    stat_pos = [db.phase_centres[stat] for stat in selected_stations]
    extended_statpos = np.array(stat_pos)[tri_idx[1]]
    refpp = getIPP(heights, stat_pos, times,
                   np.zeros((len(stat_pos), times.shape[0])) * u.s,
                   source)   #time x height x 3 

    drefpp = getIPP(heights, extended_statpos,
                    Time(times), delays * u.s, source)
    
    etrs_to_enu_matrix = etrs_to_local_north(refpp[0])
    pp1 = transform(refpp, refpp[0], etrs_to_enu_matrix)[...,:2]
    pp2 = transform(drefpp, refpp[0], etrs_to_enu_matrix)[...,:2]
    uvpp = pp2 - pp1[tri_idx[0]] # position difference between second and first station. if positive and delay positive the wave moves to EN
    A = uvpp.swapaxes(-2,0)
    w = np.array(crval)[tri_idx[0], tri_idx[1]]**2
    w[delays == 0] = 0
    w[w < 0.5] = 0
    delay_flags = np.sum(w, axis=0) < 1
    w[:, np.sum(w, axis=0) < 1]=1
    w = w.T[:,np.newaxis,:,np.newaxis]*np.eye(A.shape[-2])
    Aw = np.matmul(A.swapaxes(-1,-2),w)

    try:
        #invV = np.matmul(np.linalg.inv(np.matmul(Aw,A)),np.matmul(Aw,delays.T[:,np.newaxis,:,np.newaxis]))[...,0]
        invV = (np.linalg.inv(Aw @ A) @ (Aw @ delays.T[:, np.newaxis, :, np.newaxis]))[..., 0]
        allvelocities_wesn = (invV / np.linalg.norm(invV, axis=-1, keepdims=True)**2)
    except:
        allvelocities_wesn = False
    return allvelocities_wesn, cross_corr_0, delays, uvpp, refpp, crval[tri_idx[0], tri_idx[1]], selected_stations
           # enV             , cross_corr0 , delays, uvpp, refpp, crval                        , selected_stations
