import json
import logging
import os
from argparse import ArgumentParser
from datetime import datetime, timedelta, timezone
from typing import Dict, Optional, Iterable, Any, Union, ByteString, AnyStr

import astropy.io.fits as fits
import h5py
import matplotlib
import numpy
from astropy.coordinates import EarthLocation, SkyCoord, AltAz
from astropy.time import Time
from astropy import units as u

matplotlib.use('agg')
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from scintillation.Calibrationlib import apply_bandpass, getMedian, getS4
from scintillation.Calibrationlib import low_pass_filter,get_velocity,interpolate_replace_nans

logging.basicConfig(format='%(asctime)s [%(levelname)s] %(message)s',
                    level=logging.INFO)


VERSION = "v2"

def decode_str(str_or_byteslike: Union[ByteString, AnyStr]):
    try:
        return str_or_byteslike.decode()
    except (UnicodeDecodeError, AttributeError):
        return str_or_byteslike


_ROOT_SELECTED_FIELDS = (
    "ANTENNA_SET",
    "CHANNELS_PER_SUBANDS",
    "CHANNEL_WIDTH",
    "CHANNEL_WIDTH_UNIT",
    "FILEDATE",
    "FILENAME",
    "FILETYPE",
    "FILTER_SELECTION",
    "NOF_SAMPLES",
    "NOTES",
    "OBSERVATION_END_MJD",
    "OBSERVATION_END_UTC",
    "OBSERVATION_ID",
    "OBSERVATION_NOF_BITS_PER_SAMPLE",
    "OBSERVATION_START_MJD",
    "OBSERVATION_START_UTC",
    "OBSERVATION_STATIONS_LIST",
    "POINT_DEC",
    "POINT_RA",
    "PRIMARY_POINTING_DIAMETER",
    "PROJECT_ID",
    "SAMPLING_RATE",
    "SAMPLING_RATE_UNIT",
    "SAMPLING_TIME",
    "SAMPLING_TIME_UNIT",
    "SUBBAND_WIDTH",
    "SUBBAND_WIDTH_UNIT",
    "TARGET",
    "TARGETS",
    "TELESCOPE",
    "TOTAL_BAND_WIDTH",
    "TOTAL_INTEGRATION_TIME",
    "TOTAL_INTEGRATION_TIME_UNIT",

)

_BEAM_FIELDS = (
    "BEAM_FREQUENCY_CENTER",
    "BEAM_FREQUENCY_MAX",
    "BEAM_FREQUENCY_MIN",
    "BEAM_FREQUENCY_UNIT",
    "BEAM_STATIONS_LIST",
    "OBSERVATION_STATIONS_LIST",
    "DYNSPEC_BANDWIDTH",
    "DYNSPEC_START_UTC",
    "DYNSPEC_STOP_UTC",
    "POINT_DEC",
    "POINT_RA",
    "point_start_azimuth",
    "point_end_azimuth",
    "point_start_elevation",
    "point_end_elevation",
    "SIGNAL_SUM",
    "STOCKES_COMPONENT",
    "TARGET",
    "TRACKING",
    "REF_LOCATION_FRAME",
    "REF_LOCATION_UNIT",
    "REF_LOCATION_VALUE",
    "REF_TIME_FRAME",
    "REF_TIME_UNIT",
    "REF_TIME_VALUE",
    "TIME",
    "SPECTRAL"
)


def mjd_to_datetime(mjd) -> datetime:
    return Time(mjd, format='mjd').to_datetime()


class SmartJsonEncoder(json.JSONEncoder):
    def default(self, o: Any) -> Any:
        try:
            if isinstance(o, numpy.int32):
                return int(o)
            elif isinstance(o, numpy.int64):
                return int(o)
            elif isinstance(o, numpy.ndarray):
                return o.tolist()
            elif isinstance(o, numpy.uint64):
                return int(o)
            elif isinstance(o, numpy.uint32):
                return int(o)
            elif isinstance(o, datetime):
                return o.isoformat()
            else:
                return super().default(o)
        except TypeError:
            print(o)
            raise Exception('Cannot convert ' + str(type(o)))


def parse_args():
    parser = ArgumentParser(description='Scintillation averaging script')
    parser.add_argument('scintillation_dataset',
                        help='Scintillation dataset [e.g. Dynspec_rebinned_L271905_SAP000.h5]')
    parser.add_argument('output_directory', help='Output directory')

    return parser.parse_args()


def open_dataset(path):
    if not os.path.exists(path):
        raise FileNotFoundError('Cannot find file at {}'.format(path))
    return h5py.File(path, mode='r')


def copy_attrs_to_dict(h5_leaf, dict_container: Optional[Dict] = None,
                       exclude_fields: Optional[Iterable] = None,
                       include_fields: Optional[Iterable] = None):
    exclude_fields_set = set(exclude_fields) if exclude_fields else None
    if dict_container is None:
        dict_container = {}

    for key, value in h5_leaf.attrs.items():
        if include_fields is not None and key not in include_fields:
            continue
        if exclude_fields_set and key in exclude_fields_set:
            continue

        if isinstance(value, datetime):
            dict_container[key] = value.isoformat()
        elif isinstance(value, list) or isinstance(value, tuple) or isinstance(value,
                                                                               numpy.ndarray):
            dict_container[key] = list(map(decode_str, value))
        else:
            dict_container[key] = decode_str(value)
    return dict_container


def parse_datetime_str(datetime_str):
    return Time(datetime_str.split(' ')[0], format='isot', scale='utc').to_datetime()


def extract_root_metadata(dataset):
    metadata = dict()
    copy_attrs_to_dict(dataset['/'], metadata, include_fields=_ROOT_SELECTED_FIELDS)
    metadata['OBSERVATION_START_UTC'] = metadata['OBSERVATION_START_UTC'].split(' ')[0]
    metadata['OBSERVATION_END_UTC'] = metadata['OBSERVATION_END_UTC'].split(' ')[0]
    if not 'TARGET' in list(metadata.keys()):
        if 'TARGETS' in list(metadata.keys()):
            metadata['TARGET']=metadata['TARGETS'][-1].split("_")[0]
    if type(metadata['TARGET']) == list:
        metadata['TARGET']=metadata['TARGET'][-1].split("_")[0]
    return metadata


COORDINATE_TO_NAME = {
    'COORDINATE_0': 'TIME',
    'COORDINATE_1': 'SPECTRAL',
}


def extract_coordinates_metadata(dataset):
    coordinates = copy_attrs_to_dict(dataset)
    for item in dataset:
        coo_dict = {}
        copy_attrs_to_dict(dataset[item], coo_dict)
        coordinates[item] = coo_dict
    return coordinates


def extract_coordinates_metadata_raw(dataset):
    coordinates = copy_attrs_to_dict(dataset)
    for item in dataset:
        renamed_item = COORDINATE_TO_NAME[item]
        item_dict = {}
        copy_attrs_to_dict(dataset[item], item_dict)
        coordinates[renamed_item] = item_dict
    return coordinates


def extract_dynspec(dataset):
    dynspec_root = {}
    copy_attrs_to_dict(dataset, dynspec_root)
    for item in dataset:
        if item == 'COORDINATES':
            coordinates = extract_coordinates_metadata(dataset[item])
            dynspec_root.update(coordinates)
        else:
            dynspec_root.update(copy_attrs_to_dict(dataset[item]))

    dynspec_root = {key: value for key, value in dynspec_root.items() if
                    key in _BEAM_FIELDS}
    return dynspec_root


def extract_dynspec_raw(dataset):
    dynspec_root = {}
    copy_attrs_to_dict(dataset, dynspec_root)
    for item in dataset:
        if item.startswith('BEAM'):
            for subitem in dataset[item]:
                if subitem == 'COORDINATES':
                    coordinates = extract_coordinates_metadata_raw(
                        dataset[item][subitem])
                    dynspec_root.update(coordinates)
                else:
                    dynspec_root.update(copy_attrs_to_dict(dataset[item]))

    dynspec_root = {key: value for key, value in dynspec_root.items() if
                    key in _BEAM_FIELDS}
    return dynspec_root


def compute_start_end_azimuth_elevation(metadata):
    x, y, z = metadata['REF_LOCATION_VALUE']
    unit, *_ = metadata['REF_LOCATION_UNIT']
    ra, dec = metadata['POINT_RA'], metadata['POINT_DEC']
    start_time = Time(metadata['sample_start_datetime'].timestamp(), format='unix')
    end_time = Time(metadata['sample_end_datetime'].timestamp(), format='unix')

    location = EarthLocation(x=x, y=y, z=z, unit=unit)
    start_altaz, end_altaz = SkyCoord(ra=ra, dec=dec, unit='deg').transform_to(
        AltAz(obstime=[start_time, end_time],
              location=location))
    metadata['point_start_azimuth'] = start_altaz.az.deg
    metadata['point_start_elevation'] = start_altaz.alt.deg

    metadata['point_end_azimuth'] = end_altaz.az.deg
    metadata['point_end_elevation'] = end_altaz.alt.deg
    return metadata

def compute_azimuth_elevation(metadata, times):
    x, y, z = metadata['REF_LOCATION_VALUE']
    unit, *_ = metadata['REF_LOCATION_UNIT']
    ra, dec = metadata['POINT_RA'], metadata['POINT_DEC']
    start_time = Time(metadata['sample_start_datetime'].timestamp(), format='unix')
    end_time = Time(metadata['sample_end_datetime'].timestamp(), format='unix')

    location = EarthLocation(x=x, y=y, z=z, unit=unit)
    azel = SkyCoord(ra=ra, dec=dec, unit='deg').transform_to(
        AltAz(obstime=times,
              location=location))
    metadata['azimuth'] = azel.az.deg
    metadata['elevation'] = azel.alt.deg
    metadata['azel_times'] = times.mjd

    return metadata


def extract_metadata(dataset):
    root_metadata = extract_root_metadata(dataset)
    metadata_per_dynspec = {}

    for dynspec in dataset['/']:
        logging.info(dynspec)
        if dynspec.startswith('DYNSPEC'):
            metadata_per_dynspec[dynspec] = extract_dynspec(dataset[dynspec])
        elif dynspec.startswith('SUB_ARRAY_POINTING'):
            metadata_per_dynspec[dynspec] = extract_dynspec_raw(dataset[dynspec])
        else:
            continue
        metadata_per_dynspec[dynspec].update(root_metadata)
    return metadata_per_dynspec


def create_fits_from_dataset(sample_info, data_array, S4data, S4data180, flags, flux,
                             bandpass, output_path):
    start_datetime = sample_info['sample_start_datetime']
    end_datetime = sample_info['sample_end_datetime']
    delta_seconds = (end_datetime - start_datetime).seconds / data_array.shape[0]

    start_freq = sample_info['sample_start_frequency']
    end_freq = sample_info['sample_end_frequency']
    delta_freq = (end_freq - start_freq) / data_array.shape[1]

    hdu_lofar = fits.PrimaryHDU()
    hdu_lofar.data = data_array.T
    hdu_lofar.header['SIMPLE'] = True
    hdu_lofar.header['BITPIX'] = 8
    hdu_lofar.header['NAXIS '] = 2
    hdu_lofar.header['NAXIS1'] = data_array.shape[0]
    hdu_lofar.header['NAXIS2'] = data_array.shape[1]
    hdu_lofar.header['EXTEND'] = True
    hdu_lofar.header['DATE'] = start_datetime.strftime("%Y-%m-%d")
    hdu_lofar.header['CONTENT'] = start_datetime.strftime(
        "%Y/%m/%d") + ' Radio Flux Intensity LOFAR ' + \
                                  sample_info['ANTENNA_SET']
    hdu_lofar.header['ORIGIN'] = 'ASTRON Netherlands'
    hdu_lofar.header['TELESCOP'] = sample_info['TELESCOPE']
    hdu_lofar.header['OBSID'] = sample_info['OBSERVATION_ID']
    hdu_lofar.header['INSTRUME'] = sample_info['ANTENNA_SET']
    hdu_lofar.header['OBJECT'] = sample_info['TARGET']
    hdu_lofar.header['DATE-OBS'] = start_datetime.strftime("%Y/%m/%d")
    hdu_lofar.header['TIME-OBS'] = start_datetime.strftime("%H:%M:%S.%f")
    hdu_lofar.header['DATE-END'] = end_datetime.strftime("%Y/%m/%d")
    hdu_lofar.header['TIME-END'] = end_datetime.strftime("%H:%M:%S.%f")

    hdu_lofar.header['BZERO'] = 0.
    hdu_lofar.header['BSCALE'] = 1.
    hdu_lofar.header['BUNIT'] = 'digits  '
    hdu_lofar.header['DATAMIN'] = numpy.nanmin(data_array)
    hdu_lofar.header['DATAMAX'] = numpy.nanmax(data_array)

    hdu_lofar.header['CRVAL1'] = start_datetime.timestamp()
    hdu_lofar.header['CRPIX1'] = 0
    hdu_lofar.header['CTYPE1'] = 'Time [UT]'
    hdu_lofar.header['CDELT1'] = delta_seconds
    hdu_lofar.header['CRVAL2'] = start_freq
    hdu_lofar.header['CRPIX2'] = 0
    hdu_lofar.header['CTYPE2'] = 'Frequency [MHz]'
    hdu_lofar.header['CDELT2'] = delta_freq
    hdu_lofar.header['RA'] = sample_info['POINT_RA']
    hdu_lofar.header['DEC'] = sample_info['POINT_DEC']
    hdu_lofar.header['REF_X'] = sample_info['REF_LOCATION_VALUE'][0]
    hdu_lofar.header['REF_Y'] = sample_info['REF_LOCATION_VALUE'][1]
    hdu_lofar.header['REF_Z'] = sample_info['REF_LOCATION_VALUE'][2]
    hdu_lofar.header['REF_UNIT'] = sample_info['REF_LOCATION_UNIT'][0]
    hdu_lofar.header['HISTORY'] = '        '
    S4_hdu = fits.ImageHDU(S4data.T, name='S4_60s')  # to do , add header info
    S4_hdu.header['CRVAL1'] = start_datetime.replace(tzinfo=timezone.utc).timestamp()
    S4_hdu.header['CRPIX1'] = 0
    S4_hdu.header['CTYPE1'] = 'Time [UT]'
    S4_hdu.header['CDELT1'] = delta_seconds
    S4_hdu.header['CRVAL2'] = start_freq
    S4_hdu.header['CRPIX2'] = 0
    S4_hdu.header['CTYPE2'] = 'Frequency [MHz]'
    S4_hdu.header['CDELT2'] = delta_freq

    S4_180hdu = fits.ImageHDU(S4data180.T, name='S4_180s')  # to do , add header info
    S4_180hdu.header['CRVAL1'] = start_datetime.timestamp()
    S4_180hdu.header['CRPIX1'] = 0
    S4_180hdu.header['CTYPE1'] = 'Time [UT]'
    S4_180hdu.header['CDELT1'] = delta_seconds
    S4_180hdu.header['CRVAL2'] = start_freq
    S4_180hdu.header['CRPIX2'] = 0
    S4_180hdu.header['CTYPE2'] = 'Frequency [MHz]'
    S4_180hdu.header['CDELT2'] = delta_freq
    flag_hdu = fits.ImageHDU(flags.T, name='flag_percentage')
    flag_hdu.header['CRVAL1'] = start_datetime.timestamp()
    flag_hdu.header['CRPIX1'] = 0
    flag_hdu.header['CTYPE1'] = 'Time [UT]'
    flag_hdu.header['CDELT1'] = delta_seconds
    flag_hdu.header['CRVAL2'] = start_freq
    flag_hdu.header['CRPIX2'] = 0
    flag_hdu.header['CTYPE2'] = 'Frequency [MHz]'
    flag_hdu.header['CDELT2'] = delta_freq
    bp_hdu = fits.ImageHDU(bandpass.T, name='bandpass')
    bp_hdu.header['CRVAL1'] = start_datetime.timestamp()
    bp_hdu.header['CRPIX1'] = 0
    bp_hdu.header['CTYPE1'] = 'Time [UT]'
    bp_hdu.header['CDELT1'] = delta_seconds
    bp_hdu.header['CRVAL2'] = start_freq
    bp_hdu.header['CRPIX2'] = 0
    bp_hdu.header['CTYPE2'] = 'Frequency [MHz]'
    bp_hdu.header['CDELT2'] = delta_freq
    flux_hdu = fits.ImageHDU(flux, name='Flux_spectrum')
    flux_hdu.header['CRVAL1'] = start_freq
    flux_hdu.header['CRPIX1'] = 0
    flux_hdu.header['CTYPE1'] = 'Frequency [MHz]'
    flux_hdu.header['CDELT1'] = delta_freq
    full_hdu = fits.HDUList([hdu_lofar, S4_hdu, S4_180hdu, flag_hdu, flux_hdu, bp_hdu])
    full_hdu.writeto(output_path, overwrite=True)


def create_fits_from_velocity(sample_info, velocity, cross_corr0, delays, uvpp, refpp,
                              crval, heights, selected_stations, output_path):
    start_datetime = sample_info['sample_start_datetime']
    end_datetime = sample_info['sample_end_datetime']
    delta_seconds = (end_datetime - start_datetime).seconds / velocity.shape[0]


    hdu_lofar = fits.PrimaryHDU()
    hdu_lofar.data = velocity.T
    hdu_lofar.header['SIMPLE'] = True
    hdu_lofar.header['BITPIX'] = 8
    hdu_lofar.header['NAXIS '] = 3
    hdu_lofar.header['NAXIS1'] = velocity.shape[0]
    hdu_lofar.header['NAXIS2'] = velocity.shape[1]
    hdu_lofar.header['NAXIS3'] = velocity.shape[2]
    hdu_lofar.header['EXTEND'] = True
    hdu_lofar.header['DATE'] = start_datetime.strftime("%Y-%m-%d")
    hdu_lofar.header['CONTENT'] = start_datetime.strftime(
        "%Y/%m/%d") + ' Fitted plasma drift valocities ' + \
                                  sample_info['ANTENNA_SET']
    hdu_lofar.header['ORIGIN'] = 'ASTRON Netherlands'
    hdu_lofar.header['TELESCOP'] = sample_info['TELESCOPE']
    hdu_lofar.header['OBSID'] = sample_info['OBSERVATION_ID']
    hdu_lofar.header['INSTRUME'] = sample_info['ANTENNA_SET']
    hdu_lofar.header['OBJECT'] = sample_info['TARGET']
    hdu_lofar.header['DATE-OBS'] = start_datetime.strftime("%Y/%m/%d")
    hdu_lofar.header['TIME-OBS'] = start_datetime.strftime("%H:%M:%S.%f")
    hdu_lofar.header['DATE-END'] = end_datetime.strftime("%Y/%m/%d")
    hdu_lofar.header['TIME-END'] = end_datetime.strftime("%H:%M:%S.%f")

    hdu_lofar.header['DATAMIN'] = numpy.nanmin(velocity)
    hdu_lofar.header['DATAMAX'] = numpy.nanmax(velocity)

    hdu_lofar.header['CRVAL1'] = start_datetime.timestamp()
    hdu_lofar.header['CRPIX1'] = 0
    hdu_lofar.header['CTYPE1'] = 'Time [UT]'
    hdu_lofar.header['CDELT1'] = delta_seconds
    hdu_lofar.header['CRVAL2'] = 0
    hdu_lofar.header['CRPIX2'] = 0
    hdu_lofar.header['CTYPE2'] = 'height index'
    hdu_lofar.header['CDELT2'] = 1
    hdu_lofar.header['CRVAL3'] = 0
    hdu_lofar.header['CRPIX3'] = 0
    hdu_lofar.header['CTYPE3'] = 'NS/EW index'
    hdu_lofar.header['CDELT3'] = 1
    
    hdu_lofar.header['RA'] = sample_info['POINT_RA']
    hdu_lofar.header['DEC'] = sample_info['POINT_DEC']
    hdu_lofar.header['HISTORY'] = '        '
    
    crosscorr_hdu = fits.ImageHDU(cross_corr0, name='cross_corr0')  # to do , add header info
    crosscorr_hdu.header['CRVAL1'] = start_datetime.replace(tzinfo=timezone.utc).timestamp()
    crosscorr_hdu.header['CRPIX1'] = 0
    crosscorr_hdu.header['CTYPE1'] = 'Time [UT]'
    crosscorr_hdu.header['CDELT1'] = delta_seconds
    crosscorr_hdu.header['CRVAL2'] = 0
    crosscorr_hdu.header['CRPIX2'] = 0
    crosscorr_hdu.header['CTYPE2'] = 'baseline index'
    crosscorr_hdu.header['CDELT2'] = 1

    delays_hdu = fits.ImageHDU(delays, name='delays')  # to do , add header info
    delays_hdu.header['CRVAL1'] = start_datetime.timestamp()
    delays_hdu.header['CRPIX1'] = 0
    delays_hdu.header['CTYPE1'] = 'Time [UT]'
    delays_hdu.header['CDELT1'] = delta_seconds
    delays_hdu.header['CRVAL2'] = 0
    delays_hdu.header['CRPIX2'] = 0
    delays_hdu.header['CTYPE2'] = 'baseline index'
    delays_hdu.header['CDELT2'] = 1

    crval_hdu = fits.ImageHDU(crval, name='crval')  # to do , add header info
    crval_hdu.header['CRVAL1'] = start_datetime.timestamp()
    crval_hdu.header['CRPIX1'] = 0
    crval_hdu.header['CTYPE1'] = 'Time [UT]'
    crval_hdu.header['CDELT1'] = delta_seconds
    crval_hdu.header['CRVAL2'] = 0
    crval_hdu.header['CRPIX2'] = 0
    crval_hdu.header['CTYPE2'] = 'baseline index'
    crval_hdu.header['CDELT2'] = 1

    uvpp_hdu = fits.ImageHDU(uvpp.swapaxes(2,0).T, name='piercepoints')
    uvpp_hdu.header['CRVAL1'] = start_datetime.timestamp()
    uvpp_hdu.header['CRPIX1'] = 0
    uvpp_hdu.header['CTYPE1'] = 'Time [UT]'
    uvpp_hdu.header['CDELT1'] = delta_seconds
    uvpp_hdu.header['CRVAL2'] = 0
    uvpp_hdu.header['CRPIX2'] = 0
    uvpp_hdu.header['CTYPE2'] = 'heights'
    uvpp_hdu.header['CDELT2'] = 1
    uvpp_hdu.header['CRVAL3'] = 0
    uvpp_hdu.header['CRPIX3'] = 0
    uvpp_hdu.header['CTYPE3'] = 'baseline index'
    uvpp_hdu.header['CDELT3'] = 1
    uvpp_hdu.header['CRVAL4'] = 0
    uvpp_hdu.header['CRPIX4'] = 0
    uvpp_hdu.header['CTYPE4'] = 'EW/NS index'
    uvpp_hdu.header['CDELT4'] = 1
    pp_hdu = fits.ImageHDU(refpp.swapaxes(2,0).T, name='piercepoints0')
    pp_hdu.header['CRVAL1'] = start_datetime.timestamp()
    pp_hdu.header['CRPIX1'] = 0
    pp_hdu.header['CTYPE1'] = 'Time [UT]'
    pp_hdu.header['CDELT1'] = delta_seconds
    pp_hdu.header['CRVAL2'] = 0
    pp_hdu.header['CRPIX2'] = 0
    pp_hdu.header['CTYPE2'] = 'heights'
    pp_hdu.header['CDELT2'] = 1
    pp_hdu.header['CRVAL3'] = 0
    pp_hdu.header['CRPIX3'] = 0
    pp_hdu.header['CTYPE3'] = 'station index'
    pp_hdu.header['CDELT3'] = 1
    pp_hdu.header['CRVAL4'] = 0
    pp_hdu.header['CRPIX4'] = 0
    pp_hdu.header['CTYPE4'] = 'EW/NS index'
    pp_hdu.header['CDELT4'] = 1


    col1 = fits.Column(name='stations', format='10A', array=numpy.array(selected_stations))
    col2 = fits.Column(name='heights', format='D', array=heights)
    coldefs = fits.ColDefs([ col1,])
    hdu_stations = fits.BinTableHDU.from_columns(coldefs)
    coldefs = fits.ColDefs([ col2,])
    hdu_heights = fits.BinTableHDU.from_columns(coldefs)

    tri_idx = numpy.tril_indices(len(selected_stations),-1)
    baseline_data = [(i,j) for i,j in zip( numpy.array(selected_stations)[tri_idx[0]], numpy.array(selected_stations)[tri_idx[1]] )  ]
    dtype = [('station1', f'U10'), ('station2', f'U10')]
    baselines = numpy.array(baseline_data, dtype=dtype)
    hdu = fits.BinTableHDU(baselines)

    full_hdu = fits.HDUList([hdu_lofar, crosscorr_hdu, delays_hdu, crval_hdu, uvpp_hdu, pp_hdu, hdu_heights, hdu_stations, hdu])
    full_hdu.writeto(output_path, overwrite=True)


def make_plot(data_array, time_axis, frequency_axis, station_name, plot_full_path,
              label="Dynspec"):
    fig = plt.figure(figsize=(6, 4), dpi=120)
    ax = plt.gca()
    start_time = datetime.fromtimestamp(time_axis[0]).strftime("%Y/%m/%d %H:%M:%S")
    datetime_axis = [datetime.fromtimestamp(time_s) for time_s in time_axis]

    times = mdates.date2num(datetime_axis)
    title = '{label} {station_name} - {start_time}'.format(label=label,
                                                           station_name=station_name,
                                                           start_time=start_time)
    data_fits_new = data_array
    vmin = 0.7
    vmax = 1.5

    im = ax.imshow(data_fits_new.T, origin='lower', interpolation='none', aspect='auto',
                   vmin=vmin,
                   vmax=vmax,
                   extent=[times[0], times[-1], frequency_axis[0], frequency_axis[-1]],
                   cmap='inferno')
    fig.colorbar(im)
    plt.suptitle(title)
    ax.xaxis_date()
    ax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    ax.set_xlim(
        [datetime(year=datetime_axis[0].year, month=datetime_axis[0].month,
                  day=datetime_axis[0].day,
                  hour=datetime_axis[0].hour),
         datetime(year=datetime_axis[0].year, month=datetime_axis[0].month,
                  day=datetime_axis[0].day,
                  hour=datetime_axis[0].hour) + timedelta(hours=1)]
    )
    ax.set_xlabel('Time (UT)')
    ax.set_ylabel('Frequency (MHz)')
    plt.savefig(plot_full_path)
    plt.close(fig)


def make_S4plot(data_array, time_axis, frequency_axis, station_name, plot_full_path,
                label="S4"):
    fig = plt.figure(figsize=(6, 4), dpi=120)
    ax = plt.gca()
    start_time = datetime.fromtimestamp(time_axis[0]).strftime("%Y/%m/%d %H:%M:%S")
    datetime_axis = [datetime.fromtimestamp(time_s) for time_s in time_axis]

    times = mdates.date2num(datetime_axis)
    title = '{label} {station_name} - {start_time}'.format(label=label,
                                                           station_name=station_name,
                                                           start_time=start_time)

    vmin = 0.
    vmax = 0.3

    im = ax.imshow(data_array.T, origin='lower', interpolation='none', aspect='auto',
                   vmin=vmin,
                   vmax=vmax,
                   extent=[times[0], times[-1], frequency_axis[0], frequency_axis[-1]],
                   cmap='plasma')
    fig.colorbar(im)
    plt.suptitle(title)
    ax.xaxis_date()
    ax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    ax.set_xlim(
        [datetime(year=datetime_axis[0].year, month=datetime_axis[0].month,
                  day=datetime_axis[0].day,
                  hour=datetime_axis[0].hour),
         datetime(year=datetime_axis[0].year, month=datetime_axis[0].month,
                  day=datetime_axis[0].day,
                  hour=datetime_axis[0].hour) + timedelta(hours=1)]
    )
    ax.set_xlabel('Time (UT)')
    ax.set_ylabel('Frequency (MHz)')
    plt.savefig(plot_full_path)
    plt.close(fig)

def make_cross_corr_plots(plot_full_path, cross_corr, uvpp, time,vel):
    fig = plt.figure(figsize=(6, 4), dpi=120)
    ax = plt.gca()
    #start_time = datetime.fromtimestamp(time).strftime("%Y/%m/%d %H:%M:%S")
    start_time = time.strftime("%Y/%m/%d %H:%M:%S")

    label = "Cross correlation (at delay 0)"
    title = '{label} - {start_time}'.format(label=label,
                                            start_time=start_time)

    vmin = 0.
    vmax = 1.
    tmp_data = numpy.copy(cross_corr)
    tmp_data[tmp_data<vmin]=vmin
    im = ax.scatter(uvpp[:,0],uvpp[:,1],c=tmp_data, cmap='hsv')
    ax.arrow(0, 0, vel[0]*3, vel[1]*3, head_width=100, color='k')
    ax.set_xlim(-2500,2500)
    ax.set_ylim(-2500,2500)
    ax.set_aspect(1)
    fig.colorbar(im)
    plt.suptitle(title)
    ax.set_xlabel("EW distance (m)")
    ax.set_ylabel("NS distance (m)")
    plt.savefig(plot_full_path)
    plt.close(fig)

def make_delay_plots(plot_full_path, delays, uvpp, time,vel):
    fig = plt.figure(figsize=(6, 4), dpi=120)
    ax = plt.gca()
    #start_time = datetime.fromtimestamp(time).strftime("%Y/%m/%d %H:%M:%S")
    start_time = time.strftime("%Y/%m/%d %H:%M:%S")

    label = "Delay"
    title = '{label} - {start_time}'.format(label=label,
                                            start_time=start_time)

    tmp_data = numpy.copy(delays)
    im = ax.scatter(uvpp[:,0],uvpp[:,1],c=tmp_data, cmap='jet')
    ax.arrow(0, 0, vel[0]*3, vel[1]*3, head_width=100, color='k')
    ax.set_xlim(-2500,2500)
    ax.set_ylim(-2500,2500)
    ax.set_aspect(1)
    fig.colorbar(im,label='delay (s)')
    plt.suptitle(title)
    ax.set_xlabel("EW distance (m)")
    ax.set_ylabel("NS distance (m)")
    plt.savefig(plot_full_path)
    plt.close(fig)
    
    

    
def make_S4_array(sample_info,S4_data_array,freq_axis,min_freq=30,max_freq=70, sample_rate=60, label='S4_data'):
    idx1 = numpy.argmin(numpy.abs(freq_axis-min_freq))
    idx2 = numpy.argmin(numpy.abs(freq_axis-max_freq))
    if idx1==idx2:
        idx2=idx1+1
    sample_info[label] = numpy.median(S4_data_array[::sample_rate,idx1:idx2],axis=1)
    
def create_averaged_dataset(sample_info, data_array, flags=None, bandpass=None):
    average_window = sample_info['average_window_samples']
    start_datetime, end_datetime = sample_info['sample_start_datetime'], sample_info[
        'sample_end_datetime']
    start_freq, end_freq = sample_info['sample_start_frequency'], sample_info[
        'sample_end_frequency']
    output_samples = sample_info['sample_time_samples']

    time_axis = numpy.linspace(start_datetime.timestamp(), end_datetime.timestamp(),
                               output_samples)
    frequency_axis = numpy.linspace(start_freq, end_freq, data_array.shape[1])

    avgdata, avgflags, avgbandpass = getMedian(data_array, average_window, axis=0,
                                               flags=flags, bandpass=bandpass,
                                               has_nan=numpy.any(
                                                   numpy.isnan(data_array)))

    return numpy.array(avgdata,
                       dtype=numpy.float32), time_axis, frequency_axis, avgflags, avgbandpass


def round_up_datetime(datet, interval):
    return datetime.fromtimestamp(numpy.ceil(datet.timestamp() / interval) * interval)


def round_down_datetime(datet, interval):
    return datetime.fromtimestamp(numpy.floor(datet.timestamp() / interval) * interval)


def split_samples(dynspec_name,
                  metadata,
                  dataset: h5py.File,
                  sample_window,
                  averaging_window,
                  out_directory):
    """

    :param dynspec_name: Dynspec tab name
    :param dataset: dynspec dataset
    :param sample_window: sample window in seconds
    :param averaging_window: averaging window in seconds

    :return:
    """
    if 'INCREMENT' in metadata['TIME'].keys():
        if isinstance(metadata['TIME']['INCREMENT'], list):
            time_delta, *_ = metadata['TIME']['INCREMENT']
        else:
            time_delta = metadata['TIME']['INCREMENT']
    else:
        time_delta = metadata['SAMPLING_TIME']

    if 'DYNSPEC_START_UTC' in metadata:
        obs_start_time = parse_datetime_str(metadata['DYNSPEC_START_UTC'])
        obs_end_time = parse_datetime_str(metadata['DYNSPEC_STOP_UTC'])
    else:
        obs_start_time = parse_datetime_str(metadata['OBSERVATION_START_UTC'])
        obs_end_time = parse_datetime_str(metadata['OBSERVATION_END_UTC'])
    if 'AXIS_VALUE_WORLD' in metadata['SPECTRAL']:
        frequency = metadata['SPECTRAL']['AXIS_VALUE_WORLD']
    else:
        frequency = metadata['SPECTRAL']['AXIS_VALUES_WORLD']
    antenna_set = metadata['ANTENNA_SET']

    start_frequency, end_frequency = frequency[0] / 1.e6, frequency[-1] / 1.e6
    if 'BEAM_STATIONS_LIST' in metadata:
        station_name, *_ = metadata['BEAM_STATIONS_LIST']
        station_name = decode_str(station_name)
    else:
        station_name, *_ = metadata['OBSERVATION_STATIONS_LIST']

    if 'DATA' in dataset[dynspec_name]:
        data_array = dataset[dynspec_name]['DATA']
        nofch = 1 #TODO check if this is always the case for DYNSPEC data
    else:
        data_array = dataset[dynspec_name]['BEAM_000']['STOKES_0']
        #take median over channels for raw data
        nofch = dataset[dynspec_name]['BEAM_000']['STOKES_0'].attrs['NOF_CHANNELS'][0]
    averaging_window_in_samples = int(numpy.ceil(averaging_window / time_delta))
    averaging_window_in_seconds = averaging_window_in_samples * time_delta
    S4_60s_window_in_samples = int(60. / time_delta)
    S4_180s_window_in_samples = int(180. / time_delta)


    total_time_samples = data_array.shape[0]

    start_obs_datetime = round_down_datetime(obs_start_time, sample_window)
    end_obs_datetime = round_up_datetime(obs_end_time, sample_window)
    time_obs = numpy.linspace(obs_start_time.timestamp(), obs_end_time.timestamp(),
                              total_time_samples)
    n_samples = int((end_obs_datetime - start_obs_datetime).seconds // sample_window)
    for i in range(n_samples):
        start_sample_datetime = round_down_datetime(
            start_obs_datetime + timedelta(seconds=sample_window * i),
            averaging_window)
        end_sample_datetime = round_up_datetime(
            start_obs_datetime + timedelta(seconds=sample_window * (i + 1)),
            averaging_window)

        indexs = \
        numpy.where(numpy.logical_and(time_obs > start_sample_datetime.timestamp(),
                                      time_obs <= end_sample_datetime.timestamp()))[0]
        start_index, end_index = indexs[0], indexs[-1]

        fname = start_sample_datetime.strftime(
            "LOFAR_%Y%m%d_%H%M%S_") + station_name + '_' + antenna_set + '_' + metadata[
                    "TARGET"].replace(' ', '-')

        full_path = os.path.join(out_directory, fname)
        logging.info('--processing sample number %s for station %s--', i, station_name)
        output_time_samples = int(numpy.ceil(len(indexs) / averaging_window_in_samples))
        sample_info = {
            'average_window_samples': averaging_window_in_samples,
            'average_window_seconds': averaging_window_in_seconds,
            'sample_time_samples': output_time_samples,
            'sample_start_datetime': datetime.fromtimestamp(time_obs[start_index]),
            'sample_end_datetime': datetime.fromtimestamp(time_obs[end_index]),
            'n_time_samples': len(indexs),
            'sample_start_frequency': start_frequency,
            'sample_end_frequency': end_frequency}
        sample_info.update(metadata)
        compute_start_end_azimuth_elevation(sample_info)
        sample_rate = int(
            averaging_window_in_samples * 3. / averaging_window_in_seconds)
        logging.info("taking median over %d channels",nofch)
        if nofch>1:
            nsb = data_array.shape[1]//nofch
            tmp_data = data_array[start_index:end_index]
            #fill again in loop since otherwise some data is filled with zeros, not understood why
            for ich in range(nofch):
                tmp_data[:,ich*nsb:(ich+1)*nsb] = data_array[start_index:end_index,ich*nsb:(ich+1)*nsb]
            tmp_data = numpy.median(tmp_data.reshape((-1,nsb,nofch)),axis=-1)
        else:
            tmp_data = data_array[start_index:end_index]
        tmp_data = tmp_data.squeeze()
        frequency_axis = numpy.linspace(start_frequency, end_frequency,
                                        tmp_data.shape[1])
        filtered_data, flags, flux, bandpass = apply_bandpass(
            tmp_data, frequency_axis,
            freqaxis=1, timeaxis=0, target=metadata["TARGET"],
            sample_rate=sample_rate,
            # sample every 3 seconds
            filter_length=600,  # window size 30 minutes
            rfi_filter_length=averaging_window_in_samples*5-1,
            #rfi_filter_length=49,
            # window size in time to prevent flagging scintillation
            flag_value=8, replace_value=1)
        S4_data_array = getS4(filtered_data,
                              window_size=S4_60s_window_in_samples,  # 60 seconds
                              skip_sample_time=averaging_window_in_samples,
                              # create S4 every averaging time
                              axis=0, has_nan=False)
        S4_data_array180 = getS4(filtered_data,
                                 window_size=S4_180s_window_in_samples,  # 180 seconds
                                 skip_sample_time=averaging_window_in_samples,
                                 # create S4 every averaging time
                                 axis=0, has_nan=False)
        averaged_data_array, time_axis, frequency_axis, flags, bandpass = \
            create_averaged_dataset(sample_info,
                                    tmp_data,
                                    flags,
                                    bandpass)  # make sure the data is shaped to contain integer of window

        make_plot(averaged_data_array,
                  time_axis, frequency_axis, station_name, full_path + '.png')
        make_S4plot(S4_data_array, time_axis, frequency_axis, station_name,
                    full_path + '_S4.png', label='S4 60s')
        make_S4plot(S4_data_array180, time_axis, frequency_axis, station_name,
                    full_path + '_S4_180.png',
                    label='S4 180s')
        create_fits_from_dataset(sample_info, averaged_data_array, S4_data_array,
                                 S4_data_array180, flags, flux,
                                 bandpass, full_path + '.fits')
        make_S4_array(sample_info,S4_data_array,frequency_axis,min_freq=30,max_freq=70,sample_rate=60)
        make_S4_array(sample_info,S4_data_array180,frequency_axis,min_freq=30,max_freq=70,sample_rate=60,label='S4_data_180')
        times = Time(time_axis, format='unix')[::60]
        compute_azimuth_elevation(sample_info, times)
        store_metadata(sample_info, full_path + '.json')


def get_velocities(metadata,
                   dataset,
                   out_directory,
                   sample_window=3600,
                   averaging_window=1,
                   heights=numpy.array([120,350,600])*u.km):
    """
    :param metadata: dynspec metadata
    :param dataset: dynspec dataset

    """
    # select core stations
    #CSdata = [i for i in  metadata if 'CS' in metadata[i]['BEAM_STATIONS_LIST'][0] and not 'CS302' in metadata[i]['BEAM_STATIONS_LIST'][0]]
    CSdata = [i for i in  metadata if 'CS' in metadata[i]['BEAM_STATIONS_LIST'][0]]
    stations = [metadata[i]['BEAM_STATIONS_LIST'][0] for i in CSdata]
    # take median data full time resolution frequencies between 40 and 70 MHz
    metadata = metadata[CSdata[0]]
    if isinstance(metadata['TIME']['INCREMENT'], list):
        time_delta, *_ = metadata['TIME']['INCREMENT']
    else:
        time_delta = metadata['TIME']['INCREMENT']

    if 'DYNSPEC_START_UTC' in metadata:
        obs_start_time = parse_datetime_str(metadata['DYNSPEC_START_UTC'])
        obs_end_time = parse_datetime_str(metadata['DYNSPEC_STOP_UTC'])
    else:
        obs_start_time = parse_datetime_str(metadata['OBSERVATION_START_UTC'])
        obs_end_time = parse_datetime_str(metadata['OBSERVATION_END_UTC'])
    if 'AXIS_VALUE_WORLD' in metadata['SPECTRAL']:
        frequency = metadata['SPECTRAL']['AXIS_VALUE_WORLD']
    else:
        frequency = metadata['SPECTRAL']['AXIS_VALUES_WORLD']
    dynspec_name = CSdata[0]
    if 'DATA' in dataset[dynspec_name]:
        data_array = dataset[dynspec_name]['DATA']
    else:
        data_array = dataset[dynspec_name]['BEAM_000']['STOKES_0']

    start_frequency, end_frequency = frequency[0] / 1.e6, frequency[-1] / 1.e6
    averaging_window_in_samples = int(numpy.ceil(averaging_window / time_delta))
    averaging_window_in_seconds = averaging_window_in_samples * time_delta
    total_time_samples = data_array.shape[0]
    start_obs_datetime = round_down_datetime(obs_start_time, sample_window)
    end_obs_datetime = round_up_datetime(obs_end_time, sample_window)
    time_obs = numpy.linspace(obs_start_time.timestamp(), obs_end_time.timestamp(),
                              total_time_samples)
    n_samples = int((end_obs_datetime - start_obs_datetime).seconds // sample_window)
    ra,dec = metadata['POINT_RA'], metadata['POINT_DEC']
    source = SkyCoord(ra=ra,dec=dec,unit="deg")
    for i in range(n_samples):
        
        start_sample_datetime = round_down_datetime(
            start_obs_datetime + timedelta(seconds=sample_window * i),
            averaging_window)
        end_sample_datetime = round_up_datetime(
            start_obs_datetime + timedelta(seconds=sample_window * (i + 1)),
            averaging_window)
        indexs = \
        numpy.where(numpy.logical_and(time_obs > start_sample_datetime.timestamp(),
                                      time_obs <= end_sample_datetime.timestamp()))[0]
        start_index, end_index = indexs[0], indexs[-1]
        timeseries = numpy.zeros((len(stations),end_index-start_index))
        fname = start_sample_datetime.strftime(
            "LOFAR_CORE_%Y%m%d_%H%M%S_") + metadata["TARGET"].replace(' ', '-')

        full_path = os.path.join(out_directory, fname)
        logging.info('--processing sample number %s for core stations--', i)
        output_time_samples = int(numpy.ceil(len(indexs) / averaging_window_in_samples))
        sample_info = {
            'average_window_samples': averaging_window_in_samples,
            'average_window_seconds': averaging_window_in_seconds,
            'sample_time_samples': output_time_samples,
            'sample_start_datetime': datetime.fromtimestamp(time_obs[start_index]),
            'sample_end_datetime': datetime.fromtimestamp(time_obs[end_index]),
            'n_time_samples': len(indexs),
            'sample_start_frequency': start_frequency,
            'sample_end_frequency': end_frequency,
            'stations':stations}
        sample_info.update(metadata)
        for istat,station_name,dynspec_name in zip(range(len(stations)),stations,CSdata):
            if 'DATA' in dataset[dynspec_name]:
                data_array = dataset[dynspec_name]['DATA']
                nofch = 1 #TODO check if this is always the case for DYNSPEC data
            else:
                data_array = dataset[dynspec_name]['BEAM_000']['STOKES_0']
                #take median over channels for raw data
                nofch = dataset[dynspec_name]['BEAM_000']['STOKES_0'].attrs['NOF_CHANNELS'][0]
             
            logging.info('--processing sample number %s for station %s--', i, station_name)
            sample_rate = int(
                averaging_window_in_samples * 3. / averaging_window_in_seconds)
            logging.info("taking median over %d channels",nofch)
            if nofch>1:
                nsb = data_array.shape[1]//nofch
                tmp_data = data_array[start_index:end_index]
                #fill again in loop since otherwise some data is filled with zeros, not understood why
                for ich in range(nofch):
                    tmp_data[:,ich*nsb:(ich+1)*nsb] = data_array[start_index:end_index,ich*nsb:(ich+1)*nsb]
                tmp_data = numpy.median(tmp_data.reshape((-1,nsb,nofch)),axis=-1)
            else:
                tmp_data = data_array[start_index:end_index]
            tmp_data = tmp_data.squeeze()
            frequency_axis = numpy.linspace(start_frequency, end_frequency,
                                            tmp_data.shape[1])
            filtered_data, flags, flux, bandpass = apply_bandpass(
                tmp_data, frequency_axis,
                freqaxis=1, timeaxis=0, target=metadata["TARGET"],
                sample_rate=sample_rate,
                # sample every 3 seconds
                filter_length=600,  # window size 30 minutes
                rfi_filter_length=averaging_window_in_samples*5-1,
                #rfi_filter_length=49,
                # window size in time to prevent flagging scintillation
                flag_value=8, replace_value=numpy.nan)
            freq_select= numpy.where(numpy.logical_and(frequency_axis>50,frequency_axis<70))[0]
            #filter bad times
            avg_flags = numpy.sum(numpy.isnan(filtered_data[:,freq_select]),axis=1)/freq_select.shape[0]
            filtered_data[avg_flags>0.4]=numpy.nan #more than 20% flagged
            meddata = numpy.nanmedian(filtered_data[:,freq_select],axis=-1)
            meddata = interpolate_replace_nans(meddata,numpy.ones(averaging_window_in_samples*10+1))
            meddata[numpy.isnan(meddata)]=1 #make sure there are really no nans
            timeseries[istat] =  low_pass_filter(meddata,sample_time=time_delta,cutoff=.5)
        enV,cross_corr0,delays,uvpp, refpp, crval, selected_stations = get_velocity(timeseries,
                                                                                    stations,
                                                                                    sample_time=time_delta,
                                                                                    sample_window=60,
                                                                                    starttime=Time(start_sample_datetime,format='datetime'),
                                                                                    source=source,
                                                                                    heights=heights)
        if type(enV)==bool or numpy.any(numpy.isnan(enV)):
            sample_info['EW_velocity']=-9999
            sample_info['NS_velocity']=-9999
        else:
            sample_info['EW_velocity'] = enV[:,:,0] #changed, was wrong before
            sample_info['NS_velocity'] = enV[:,:,1]
            idx_half_time = cross_corr0.shape[-1]//2
            halftime = start_sample_datetime + timedelta(seconds=sample_window * 0.5)
            time_obs[time_obs.shape[0]//2]
            make_cross_corr_plots(full_path+"_cross_corr.png",
                                  cross_corr0[:,idx_half_time],
                                  uvpp[:,1,idx_half_time],
                                  halftime,
                                  enV[idx_half_time,1])
            make_delay_plots(full_path+"_delay.png",
                             delays[:,idx_half_time],
                             uvpp[:,1,idx_half_time],
                             halftime,
                             enV[idx_half_time,1])
            sample_info['velocity altitudes'] = heights.to(u.km).value
            create_fits_from_velocity(sample_info, enV, cross_corr0, delays, uvpp, refpp,
                                      crval, heights.to(u.km).value, selected_stations, full_path + '.fits')
        del timeseries
        del delays
        del cross_corr0
        del uvpp
        del crval
        del selected_stations
        store_metadata(sample_info,full_path + '.json')
        

def store_metadata(metadata, path, version=VERSION ):
    metadata['version'] = version
    with open(path, 'w') as fout:
        json.dump(metadata, fout, cls=SmartJsonEncoder, indent=True)
