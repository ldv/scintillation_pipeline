#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: Workflow

requirements:
- class: ScatterFeatureRequirement

inputs:
- id: surls
  type: string[]
- id: samples_size
  doc: Sample sizes in seconds
  type: float?
- id: averaging_window
  doc: Averaging window in seconds
  type: float?
- id: export_stations
  doc: List of station to exports if not specified exports all the stations
  type: string[]?

outputs:
- id: metadata_json
  type: File
  outputSource:
  - compress_metadata/compressed
- id: fits_files
  type: File
  outputSource:
  - tar_fits/compressed
- id: preview_png
  type: File
  outputSource:
  - compress_preview_png/compressed

steps:
- id: fetch_data
  in:
  - id: surl_link
    source: surls
  scatter:
  - surl_link
  run: ./steps/fetch_data.cwl
  out:
  - id: beamformed_file
- id: merge
  in:
  - id: nested_file_list
    source: fetch_data/beamformed_file
  run: steps/merge_flat.cwl
  out:
  - id: flattened_file_list
- id: generate_previews
  in:
  - id: dynspec_dataset
    source: merge/flattened_file_list
    linkMerge: merge_flattened
  - id: averaging_window
    source: averaging_window
  - id: samples_size
    source: samples_size
  - id: export_stations
    source: export_stations
  scatter:
  - dynspec_dataset
  run: ./steps/extract_previews.cwl
  out:
  - id: fits_files
  - id: metadata_json
  - id: preview_png
- id: flatten_fits_files
  in:
  - id: nested_file_list
    source: generate_previews/fits_files
  run: ./steps/merge_flat.cwl
  out:
  - flattened_file_list
- id: compress_fits
  in:
  - id: file
    source: flatten_fits_files/flattened_file_list
  run: ./steps/compress_image.cwl
  scatter: 
  - file
  out:
  - compressed
- id: tar_fits
  in:
  - id: files
    source: compress_fits/compressed
  - id: type
    valueFrom: fits_files
  run: ./steps/compress.cwl
  out:
  - compressed
- id: flatten_metadata_files
  in:
  - id: nested_file_list
    source: generate_previews/metadata_json
  run: ./steps/merge_flat.cwl
  out:
  - flattened_file_list
- id: compress_metadata
  in:
  - id: files
    source: flatten_metadata_files/flattened_file_list
  - id: type
    valueFrom: metadata_json
  run: ./steps/compress.cwl
  out:
  - compressed
- id: flatten_preview_files
  in:
  - id: nested_file_list
    source: generate_previews/preview_png
  run: ./steps/merge_flat.cwl
  out:
  - flattened_file_list
- id: compress_preview_png
  in:
  - id: files
    source: flatten_preview_files/flattened_file_list
  - id: type
    valueFrom: preview_png
  run: ./steps/compress.cwl
  out:
  - compressed
