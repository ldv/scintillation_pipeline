#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: CommandLineTool

requirements:
- class: NetworkAccess
  networkAccess: true
- class: InitialWorkDirRequirement
  listing:
  - entry: $(inputs.dynspec_dataset)

inputs:
- id: dynspec_dataset
  type: File
  secondaryFiles:
  - pattern: $(self.nameroot).raw
    required: false
  inputBinding:
    position: -1
- id: samples_size
  doc: Sample sizes in seconds
  type: float?
  default: 3600
  inputBinding:
    prefix: --samples_size
- id: averaging_window
  doc: Averaging window in seconds
  type: float?
  default: 1
  inputBinding:
    prefix: --averaging_window
- id: export_stations
  type: string[]?
  inputBinding:
    prefix: --export_stations

outputs:
- id: metadata_json
  type: File[]?
  outputBinding:
    glob: out/*.json
- id: fits_files
  type: File[]?
  outputBinding:
    glob: out/*.fits
- id: preview_png
  type: File[]?
  outputBinding:
    glob: out/*.png

baseCommand:
- scintillation_utils.py
arguments:
- out

hints:
- class: DockerRequirement
  dockerPull: git.astron.nl:5000/ldv/scintillation_pipeline
successCodes:
- 0
- 5
