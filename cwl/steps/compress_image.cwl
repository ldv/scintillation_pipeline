#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: CommandLineTool
label: compress fits
doc: Compress the fits with lzma

requirements:
  InitialWorkDirRequirement:
    listing:
     - entry: $(inputs.file)

inputs:
- id: file
  type: File
  inputBinding:
    position: 1

outputs:
- id: compressed
  type: File
  outputBinding:
    glob:
    - $(inputs.file.basename).xz

baseCommand:
- xz 
arguments:
- -kfz
id: compress_fits
