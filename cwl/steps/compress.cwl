#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: CommandLineTool
label: compress
doc: Tar a list of files

requirements:
  InitialWorkDirRequirement:
    listing:
    - entryname: compress.sh
      entry: |
        #!/bin/bash
        mkdir -p $(inputs.type)
        cp $@ $(inputs.type)
        tar -cvf $(inputs.type).tar $(inputs.type)
  InlineJavascriptRequirement: {}

inputs:
- id: files
  type: File[]
  inputBinding:
    position: 1
- id: type
  type: string

outputs:
- id: compressed
  type: File
  outputBinding:
    glob:
    - $(inputs.type).tar

baseCommand:
- bash
- compress.sh
id: fetchdata
