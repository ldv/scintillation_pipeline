#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: CommandLineTool
label: fetch_data
doc: Untar a compressed file

requirements:
  InitialWorkDirRequirement:
    listing:
    - entryname: fetch.sh
      entry: |
        #!/bin/bash
        mkdir out
        cd out

        echo "Downloading $1"
        if [[ "$1" == *"lta-head.lofar.psnc.pl"* ]]; then
            cd ..
            wget --no-check-certificate --read-timeout=50 --timeout 50 https://lta-download.lofar.psnc.pl/lofigrid/SRMFifoGet.py\?surl\=$1  
            cd out
        else

            gfal-copy $1 ..
        fi
        tar -xvf ../*.tar
  InlineJavascriptRequirement: {}

inputs:
- id: surl_link
  type: string
  inputBinding:
    position: 0

outputs:
- id: beamformed_file
  type: File[]
  secondaryFiles:
  - pattern: $(self.nameroot).raw
    required: false
  outputBinding:
    glob:
    - out/*/Dynspec_*.h5
    - out/*.h5

baseCommand:
- bash
- fetch.sh
id: fetchdata
