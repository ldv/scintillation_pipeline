import unittest
import os
import tempfile
from glob import glob
from scintillation.averaging import open_dataset, extract_metadata, split_samples

basepath = os.path.dirname(__file__)
test_datasets = glob(os.path.join(basepath, 'data', '*.h5'))


def is_test_data_present():
    return len(test_datasets) > 0


class TestMetadata(unittest.TestCase):
    @unittest.skipUnless(is_test_data_present(), 'missing test data')
    def test_preview_generation(self):
        with tempfile.TemporaryDirectory() as output_dir:
            dataset = open_dataset(test_datasets[0])
            metadata = extract_metadata(dataset)
            dynspec, *_ = metadata.keys()
            split_samples(dynspec, metadata[dynspec], dataset, 3600, 10, output_dir)

            out_fits_files = glob(os.path.join(output_dir, '*.fits'))
            out_png_files = glob(os.path.join(output_dir, '*.png'))
            out_json_files = glob(os.path.join(output_dir, '*.json'))

            self.assertTrue(len(out_fits_files) > 0, msg='Fits files not generated')
            self.assertTrue(len(out_png_files) > 0, msg='PNG files not generated')
            self.assertTrue(len(out_json_files) > 0, msg='Json files not generated')
