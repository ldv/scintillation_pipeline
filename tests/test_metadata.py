import unittest
import os
import json
from glob import glob
from scintillation.averaging import open_dataset, extract_metadata, compute_start_end_azimuth_elevation

basepath = os.path.dirname(__file__)
test_datasets = glob(os.path.join(basepath, 'data', '*.h5'))
test_data_file_path = os.path.join(basepath, 'test_metadata.json')


def is_test_data_present():
    return len(test_datasets) > 0


class TestMetadata(unittest.TestCase):

    @unittest.skipUnless(is_test_data_present(), 'test data is missing')
    def test_fields_name(self):
        with open(test_data_file_path, 'r') as fin:
            expected_metadata_dict = json.load(fin)

        self.assertTrue(len(expected_metadata_dict), 'cannot load test metadata dict')
        dataset = open_dataset(test_datasets[0])
        metadata_per_dynspec = extract_metadata(dataset)
        metadata, *_ = metadata_per_dynspec.values()
        self.assertCountEqual(metadata.keys(), expected_metadata_dict.keys())


if __name__ == '__main__':
    unittest.main()
