#!/bin/bash
set -e
SOURCE=${BASH_SOURCE[0]}
TEST_DIR="$(dirname $(realpath ${SOURCE}))/data"

echo "The data will be downloaded at " ${TEST_DIR}
if [ ! -d ${TEST_DIR} ]
then
  echo "creating data directory" ${TEST_DIR}
  mkdir ${TEST_DIR}
fi

echo "changing to data directory" ${TEST_DIR}
cd ${TEST_DIR}
echo "current directory is " $PWD

# SET user and pass in ~/.wgetrc to be able to download the data
wget --recursive -q --no-parent -nH --reject="index.html*" --cut-dirs=4 https://sdc-dev.astron.nl/files/test_data/scintillation_dataset/
