import setuptools
from setuptools import setup
import os

requirements_file_path = os.path.join(os.path.dirname(__file__), 'requirements.txt')
install_requires = []
with open(requirements_file_path, 'r') as f_in:
    install_requires = f_in.read().split('\n')

setup(
    name='scintillation_tools',
    version='0.1',
    packages=setuptools.find_packages(),
    url='https://git.astron.nl/ldv/scintillation_pipeline',
    license='Apache 2.0',
    author='Mattia Mancini',
    author_email='mancini@astron.nl',
    description='Utils to process scintillation h5 files',
    test_suite='tests',
    scripts=['bin/scintillation_utils.py'],
    install_requires=install_requires
)
