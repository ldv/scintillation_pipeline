#!/usr/bin/python3
from argparse import ArgumentParser
import os
import logging
import scintillation.averaging as averaging

logging.basicConfig(format='%(asctime)s [%(levelname)s] %(message)s', level=logging.INFO)


def parse_args():
    parser = ArgumentParser(description='Scintillation averaging script')
    parser.add_argument('scintillation_dataset', help='Scintillation dataset [e.g. Dynspec_rebinned_L271905_SAP000.h5]')

    parser.add_argument('output_directory', help='Output directory')

    parser.add_argument('--samples_size', help='Samples size in seconds', default=3600, type=float)
    parser.add_argument('--averaging_window', help='Averaging window in seconds', default=1, type=float)
    parser.add_argument('--export_stations', help='Selects stations to be exported', default=None, type=str, nargs='+')
    return parser.parse_args()


def main():
    args = parse_args()
    try:
        dataset = averaging.open_dataset(args.scintillation_dataset)
    except Exception as e:
        logging.exception(e)
        logging.error('Cannot process current file')
        raise SystemExit(5)
    metadata = averaging.extract_metadata(dataset)
    os.makedirs(args.output_directory, exist_ok=True)
    if args.export_stations:
        stations = args.export_stations
    else:
        stations = []
    CSstations=0
    for dynspec in metadata:
        if'BEAM_STATIONS_LIST' in metadata[dynspec].keys() and \
          'CS' in metadata[dynspec]['BEAM_STATIONS_LIST'][0]:
            CSstations+=1
        if len(stations) and not(metadata[dynspec]['BEAM_STATIONS_LIST'][0] in stations):
            continue
        averaging.split_samples(dynspec,
                                metadata[dynspec],
                                dataset, args.samples_size, args.averaging_window, args.output_directory)
    if CSstations>3:
        averaging.get_velocities(metadata,
                                 dataset,
                                 args.output_directory,
                                 args.samples_size,
                                 args.averaging_window)
        
if __name__ == '__main__':
    main()
