#!/h5_to_fits/env python
from argparse import ArgumentParser
import pandas
import re

import requests
from sqlalchemy import create_engine
from typing import Dict, Union, List
import sys
import logging
import os.path
from configparser import ConfigParser
from urllib.parse import urlparse
import urllib.parse

logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG)

CONNECTION_STRING = 'oracle+cx_oracle://{user}:{password}@db.lofar.target.rug.nl/?service_name=db.lofar.target.rug.nl'
DEFAULT_CONFIG_PATHS = ['~/.config/solar_processing/config.ini', '~/.awe/Environment.cfg']


def parse_args():
    parser = ArgumentParser(description='Query the LTA to obtain the list of observation')
    parser.add_argument('--config', help='path to configuration file', type=str)
    parser.add_argument('workflow', help='workflow uri', type=str)
    parser.add_argument('filter', help='filter string', type=str)
    parser.add_argument('--averaging_window', help='size of window in s', type=float, default=1)
    parser.add_argument('--stations', help='list of stations', type=str, nargs="+")
    parser.add_argument('--sap_id', help='specify sapid', type=int)
    parser.add_argument('--obs_filter', help='specify specific observation string (e.g. B000_S0)')
    parser.add_argument('--dynspec', help='only select data in dynspec format',action='store_true')
    parser.add_argument('sas_ids', help='list of SAS IDS', nargs='+')
    return parser.parse_args()


logger = logging.getLogger('Solar query LTA')

def create_query_string(obs_filter=None,sap_id=None,dynspec=False):
    QUERY_STR = '''
    SELECT FO.OBJECT_ID AS OBJECT_ID,
    FO.FILESIZE AS FILESIZE,
    ST.OBS_ID as OBS_ID,
    FO."+PROJECT" AS PROJECT,
    STR.PRIMARY_URL AS PRIMARY_URL FROM AWOPER.FILEOBJECT FO JOIN
    AWOPER.STORAGETICKETRESOURCE STR ON FO.STORAGE_TICKET_RESOURCE=STR.OBJECT_ID JOIN
    AWOPER.STORAGETICKET ST ON ST.OBJECT_ID=STR.TICKET
    WHERE OBS_ID = '{sasid}' '''
    if dynspec:
        QUERY_STR += '''AND STR.PRIMARY_URL LIKE '%/LOFAR_DYNSPEC%' '''
    if obs_filter is not None:
         QUERY_STR += f'''AND STR.PRIMARY_URL LIKE '%{obs_filter}%' '''
    if sap_id is not None:
        QUERY_STR += f'''AND STR.PRIMARY_URL LIKE '%SAP{sap_id:03d}%' '''
    return QUERY_STR


def read_user_credentials(file_paths: Union[str, List[str]]) -> Dict:
    file_paths = list(map(os.path.expanduser, map(os.path.expandvars, file_paths)))
    parser = ConfigParser()
    parser.read(file_paths)
    credentials = {'user': parser['global']['database_user'], 'password': parser['global']['database_password']}
    return credentials


def read_atdb_parameters(file_paths: Union[str, List[str]]) -> Dict:
    file_paths = list(map(os.path.expanduser, map(os.path.expandvars, file_paths)))

    parser = ConfigParser()
    parser.read(file_paths)
    atdb_parameters = {'url': parser['atdb']['url'], 'token': parser['atdb']['token']}
    return atdb_parameters


def create_db_engine(user: str, password: str):
    connection_string: str = CONNECTION_STRING.format(user=urllib.parse.quote_plus(user),
                                                      password=urllib.parse.quote_plus(password))

    return create_engine(connection_string)


def find_surl_and_size_per_sasid(engine, sasid, QUERY_STR):
    result_df = pandas.read_sql_query(QUERY_STR.format(sasid=sasid), engine)
    return result_df


def parse_surl_for_info(surl):
    parsed = urlparse(surl)
    site = parsed.hostname
    path = parsed.path
    pattern = r'^.*/projects\/(?P<project>.*\d*)\/(?P<sas_id>\w\d*)\/'
    groups = re.match(pattern, path).groupdict()

    result = dict()
    for key, value in groups.items():
        result[key] = value
    return site, result


def create_payload_from_entry(entry, filter_str, workflow, avg_window, stations, purge_policy='no', predecessor=None):
    size = entry['filesize']
    site, payload = parse_surl_for_info(entry['primary_url'])
    inputs_payload = {
        'surls':
        [{'size': size,
         'surl': entry['primary_url'],
         'type': 'File',
          'location': site}],
        'averaging_window': avg_window,
        'export_stations': stations
    }

    ## default values
    payload['task_type'] = 'regular'
    payload['filter'] = filter_str
    payload['purge_policy'] = purge_policy
    payload['status'] = 'defined'
    payload['new_workflow_uri'] = workflow

    ## from entry
    payload['size_to_process'] = size
    payload['inputs'] = inputs_payload
    if predecessor:
        payload['predecessor'] = predecessor
    return payload


class AInterface:
    def __init__(self, url, token):
        self._url = url.rstrip('/')
        self._token = token
        self._session = None

    def open_session(self):
        self._session = requests.Session()
        self._session.headers['Authorization'] = f'Token {self._token}'

    def session(self) -> requests.Session:
        if self._session is None:
            self.open_session()
        return self._session

    def submit_task(self, payload):
        response = self.session().post(f'{self._url}/tasks/', json=payload)
        if not response.ok:
            logging.error('Error posting payload - %s - %s : %s', response.status_code,
                          response.reason, response.content)
            logging.error('Payload is %s', payload)
            raise SystemError()


def main():
    args = parse_args()
    sas_ids = args.sas_ids
    credentials = read_user_credentials(DEFAULT_CONFIG_PATHS)
    atdb_parameters = read_atdb_parameters(DEFAULT_CONFIG_PATHS)
    atdb_interface = AInterface(atdb_parameters['url'], atdb_parameters['token'])
    engine = create_db_engine(**credentials)
    QUERY_STR = create_query_string(obs_filter=args.obs_filter,sap_id=args.sap_id,dynspec=args.dynspec)
    for sas_id in sas_ids:
        sas_id = int(sas_id)
        logger.info('fetching surl for sas_id %s', sas_id)
        table = find_surl_and_size_per_sasid(engine, sas_id,QUERY_STR)
        for index, line in table.iterrows():
            logging.info('creating task for sas_id %s - dataset %s', sas_id, index)
            payload = create_payload_from_entry(line, args.filter, args.workflow, args.averaging_window, args.stations)
            atdb_interface.submit_task(payload)
            break #only one dataset

if __name__ == '__main__':
    main()
