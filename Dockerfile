FROM ubuntu:focal

RUN apt update && \
    DEBIAN_FRONTEND=noninteractive apt install -y \
    nodejs\
    python3 python3-matplotlib \
    python3-astropy \
    python3-numpy \
    python3-numba \
    python3-scipy \
    python3-h5py \
    python3-setuptools \
    python3-pip \
    wget


RUN pip3 install cwltool
RUN pip3 install lofarantpos

COPY . /src

RUN cd /src && python3 setup.py install