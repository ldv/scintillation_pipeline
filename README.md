# Scintillation Pipeline

## Description
The Scintillation Pipeline produces from LOFAR scintillation dataproducts a set of previews and metadata
able to describe the observation characteristics.


## Install the tool locally
To use the tool to inspect a Dynspec dataset locally you can install it
by executing the following command

```bash
python setup.py install 
```

It is suggested to run it in a virtualenv 

## How to run the command
Once installed you can find the help of the command by typing:
```bash
$ scintillation_utils.py --help
usage: scintillation_utils.py [-h] [--samples_size SAMPLES_SIZE] [--averaging_window AVERAGING_WINDOW] [--export_only_station EXPORT_ONLY_STATION] scintillation_dataset output_directory

Scintillation averaging script

positional arguments:
  scintillation_dataset
                        Scintillation dataset [e.g. Dynspec_rebinned_L271905_SAP000.h5]
  output_directory      Output directory

optional arguments:
  -h, --help            show this help message and exit
  --samples_size SAMPLES_SIZE
                        Samples size in seconds
  --averaging_window AVERAGING_WINDOW
                        Averaging window in seconds
  --export_only_station EXPORT_ONLY_STATION
```

For example if you want to process the dataset **dynspec.h5** and save the result in a directory named 
**out_dynspect** you can just run the command
```bash
$ scintillation_utils.py dynspec.h5 out_dynspect
```

## How to run the pipeline

If you want to run the CWL pipeline to dowload and process the dataset this is the command to execute

```bash
cwltool cwl/scintillation_preview_generation.cwl --surls srm://lta_site_url/path/filename.tar 
```
Other optional parameters can be specified a description is printed by executing the command

```bash
cwltool cwl/scintillation_preview_generation.cwl --help 
```
_Note that to download the data from srm url you use a GRID proxy.
Check this [link](https://drupal.star.bnl.gov/STAR/book/export/html/3182) for reference._